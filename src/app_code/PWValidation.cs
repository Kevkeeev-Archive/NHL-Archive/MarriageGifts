using System;
using System.Collections.Generic;
using System.Web;
using WebMatrix.Data;

public class PWValidation {

    public bool isValidPassword(string password){
        return longEnough(password) && hasUppercase(password) && hasLowercase(password) && hasDigit(password) && hasSpecialCharacter(password);
    }

	public bool longEnough(string password) {
		const int MIN_LENGTH = 8;
		const int MAX_LENGTH = Int32.MaxValue;

		if (password.Length >= MIN_LENGTH && password.Length <= MAX_LENGTH) {
			return true;
		} else {
			return false;
		}
	}

	public bool hasUppercase(string password) {
		foreach (char c in password) {
			if (char.IsUpper(c)) {
				return true;
			}
		}
		return false;
	}

	public bool hasLowercase(string password){      
		foreach (char c in password) {
			if (char.IsLower(c)) {
				return true;
			}
		}
		return false;
	}

	public bool hasDigit(string password) {
		foreach (char c in password) {
			if (char.IsDigit(c)) {
				return true;
			}
		}
		return false;
	}

	public bool hasSpecialCharacter(string password)	{
		foreach (char c in password) {
			if (char.IsPunctuation(c)) {
				return true;
			}
		}
		return false;
	}

    public bool hasBloodSacrifice(string password) {
        if(password.Contains("blood")) {
            return true;
        } else {
            return false;
        }
    }
    //seriously, password requirements are getting ridiculous these days.
}